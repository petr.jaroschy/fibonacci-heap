# Jaroschy

Petr Jaroschy

# Build & run

> * make > result.txt

or

> * Open solution in visual studio
> * Run

Note: runs for about a minute on mediocre hw.

# Performance conclusion

Once compiler optimizations kick in, array based binary heap outperforms the Fibonacci heap by significant margin. Memory consumption is also bad for the Fibonacci heap, especially on 64bit configuration.

One node of Fibonacci heap takes:

* 8 bytes per pointer, while having 4 pointers
* 8 bytes for priority
* likely 4 or 8 bytes for templated payload

One node of Binary heap takes:

* 8 bytes for priority
* 8 bytes for index holding (possibly unnecessary for some implementation)
* likely 4 or 8 bytes for templated payload

Comparing both heaps yields 48 bytes to 24 bytes ratio.

Binary heap benefits greatly from its array implementation, accessing elements is very fast and compiler optimizations within heap logic make binary heap superior for most uses.

Some very efficient implementations of Fibonacci heap have proven faster for some workloads, like extremely dense graph path finding with Djikstra's algorithm.

# Fibbonaci heap documentation

Fibonacci heap is a data scturcture with great theoretical performance thanks to amortized complexities. Namely insert, merge and decrease in O(1). Extracting from Fibonacci heap is still done in amortized O(log n).

## Implementation

This section is meant for implementation remarks for the solution and I will not explain how Fibonacci heap works in detail. I will only explain technical details of my implementation.

### Node

Node in Fibonacci heap is supposed to have:

* a list of children
* parent
* priority
* actual payload of the node

Technically I have implemented said variables by following:

* pointer to first child
* pointer to right and left brothers (as a cyclic linked list)
* int for priority
* templated payload of the node

Choice of making list of children of a node a linked list comes from a need to connect said lists together, like during merging two heaps together in O(1). This could improve by using data structures for children that do not have continuous memory allocation but provide O(1) merges and access to elements.

### Heap

Heap consists of nodes. Main meta_node acts as parent to all actual root nodes of Fibonacci heap and is used to check, whether a node is a root node.

Every Fibonacci heap needs to perform a consolidation of itself every extract_min call. This is done via merging root nodes into nodes of higher rank. Not unlike other implementation, my implementation uses buckets for merging said nodes.

Supportive methods were implemented to ease up the handling of nodes in such complex structure. These methods include:

* join: joins two nodes in a cylcil linked list
* cut: performs cut off during decrease operation
* add_child: adds new child to an existing node
* max_rank: returns maximum possible rank creatable by consolidate method (equivalent to fibonacci^-1(num_of_elements))
* consolidate: merges trees of same rank together until there is no two trees of same rank

Functional public methods are:

* pop_min:
  1. Search minimum of root nodes
  2. Remove minimal node and add all children of minimal node to root nodes
  3. Consolidate the heap
  4. Return pair of payload and prio while deleting node
* push:
  1. Add new root node with given priority and payload
* peek_min:
  1. Search root trees for minimum
  2. Consolidate heap
  3. Return pair of payload and prio
* decrease:
  1. Decrease nodes priority
  2. Cut node and add it as root node
  3. If parent is not mark, mark it, else cut parent recursively, until root or non marked node is found

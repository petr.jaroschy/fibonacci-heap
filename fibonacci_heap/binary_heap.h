#pragma once
#include <vector>
#include <utility>

template<typename T>
struct BinaryHeap;

template<typename T>
struct BinNode {
    T payload;
    int priority() const { return prio; }
	
private:
    int prio;
    size_t index;

    BinNode(int p, T payload, size_t index) : payload(payload), prio(p), index(index) {}

    friend struct BinaryHeap<T>;
};

template<typename T>
struct BinaryHeap
{

    bool is_empty() { return heap.size() == 0; }

    size_t size() { return heap.size(); }

    /*
        Inserts a new node with given priority and payload
     */
    BinNode<T>* push(int prio, T payload = T()) {
        BinNode<T> *n = new BinNode<T>(prio, payload, heap.size());
        heap.push_back(n);
        size_t indexCurrent = heap.size();
        while (indexCurrent > 1 && heap[indexCurrent / 2 - 1]->prio > heap[indexCurrent - 1]->prio)
        {
            std::iter_swap(heap.begin() + indexCurrent - 1, heap.begin() + indexCurrent / 2 - 1);
            std::swap(heap[indexCurrent - 1]->index, heap[indexCurrent / 2 - 1]->index);
            indexCurrent /= 2;
        }

        return n;
    }

    /*
        Removes the lowest priority node
        Returns pair of payload and priority of node with lowest priority
     */
    std::pair<T, int> pop_min() {
        std::iter_swap(heap.begin(), heap.end() - 1);
        std::swap(heap[0]->index, heap[heap.size() - 1]->index);
        BinNode<T>* to_return = heap[heap.size() - 1];
        heap.pop_back();

        size_t index = 1;
        while (true)
        {
            size_t left_index = index * 2;
            size_t right_index = left_index + 1;

            if (heap.size() >= right_index)
            {
                if (heap[left_index - 1]->prio > heap[index - 1]->prio&& heap[right_index - 1]->prio > heap[index - 1]->prio)
                {
                    break;
                }

                if (heap[left_index - 1]->prio > heap[right_index - 1]->prio)
                {
                    std::iter_swap(heap.begin() + right_index - 1, heap.begin() + index - 1);
                    std::swap(heap[right_index - 1]->index, heap[index - 1]->index);
                    index = right_index;
                }
                else
                {
                    std::iter_swap(heap.begin() + left_index - 1, heap.begin() + index - 1);
                    std::swap(heap[left_index - 1]->index, heap[index - 1]->index);
                    index = left_index;
                }
            }
            else if (heap.size() == left_index)
            {
                if (heap[left_index - 1]->prio < heap[index - 1]->prio)
                {
                    std::iter_swap(heap.begin() + left_index - 1, heap.begin() + index - 1);
                    std::swap(heap[left_index - 1]->index, heap[index - 1]->index);
                    index = left_index;
                }
                else 
                    break;
            }
            else
                break;
        }

        auto pair = std::pair<T, int>(to_return->payload, to_return->prio);

        delete(to_return);
    	
        return pair;
    }

    /*
        Returns pair of payload and priority of node with lowest priority
     */
    BinNode<T>* peek_min() {
        return heap[0];
    }

    /*
        Decrease priority of node to new_prio.
        new_prio must not be higher than node.prio.
     */
    void decrease(BinNode<T>* node, int new_prio) {
        size_t indexCurrent = node->index + 1;
        node->prio = new_prio;
        while (indexCurrent > 1 && heap[indexCurrent - 1]->prio < heap[indexCurrent / 2 - 1]->prio)
        {
            std::iter_swap(heap.begin() + indexCurrent - 1, heap.begin() + indexCurrent / 2 - 1);
            std::swap(heap[indexCurrent - 1]->index, heap[indexCurrent / 2 - 1]->index);
            indexCurrent /= 2;
        }
    }

    BinaryHeap() : heap() {}

    ~BinaryHeap()
    {
	    for (BinNode<T>* node : heap)
	    {
            delete(node);
	    }
    }
protected:
    std::vector<BinNode<T>*> heap;
};

#include <iostream>
#include <cstdio>
#include <iostream>
#include "fibonacci_heap.h"
#include "binary_heap.h"
#include <cmath>
#include <random>
#include <chrono>
#include <vector>

size_t MAX_POW = 16;

void testFibonacci()
{

	for (size_t i = 1; i < MAX_POW; ++i)
	{
		auto heap = FibonacciHeap<int>();
		auto start = std::chrono::steady_clock::now();
		const size_t n = static_cast<size_t>(pow(2, i));

		std::minstd_rand0 engine(142857);

		std::vector<Node<int>*> nodes;

		// populate heap
		for (size_t j = 0; j < n; ++j)
		{
			const int element = engine();
			nodes.push_back(heap.push(abs(element), abs(element)));
		}

		// insert, decrease, pop n elements
		for (size_t j = 0; j < n; ++j)
		{
			const int element = engine();
			Node<int>* node = heap.push(abs(element), abs(element));
			// decrease fourth of the elements 
			for (size_t j = 0; j < n / 4; ++j)
			{
				Node<int>* fib_node = nodes[engine() % nodes.size()];
				heap.decrease(fib_node, fib_node->priority() / 2);
			}
			heap.decrease(node, -1);
			heap.pop_min();
		}

		auto end = std::chrono::steady_clock::now();

		std::cout << n << "\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << "ns"
			<< std::endl;
	}
}

void testBinary()
{
	for (size_t i = 1; i < MAX_POW; ++i)
	{
		auto heap = BinaryHeap<int>();
		auto start = std::chrono::steady_clock::now();
		const size_t n = static_cast<size_t>(pow(2, i));

		std::minstd_rand0 engine(142857);

		std::vector<BinNode<int>*> nodes;

		// populate heap
		for (size_t j = 0; j < n; ++j)
		{
			const int element = engine();
			nodes.push_back(heap.push(abs(element), abs(element)));
		}

		// insert, decrease, pop n elements
		for (size_t j = 0; j < n; ++j)
		{
			const int element = engine();
			BinNode<int>* node = heap.push(abs(element), abs(element));
			// decrease fourth of the elements 
			for (size_t j = 0; j < n / 4; ++j)
			{
				BinNode<int>* fib_node = nodes[engine() % nodes.size()];
				heap.decrease(fib_node, fib_node->priority() / 2);
			}
			heap.decrease(node, -1);
			heap.pop_min();
		}

		auto end = std::chrono::steady_clock::now();

		std::cout << n << "\t" << std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count() << "ns"
			<< std::endl;
	}
}

int main()
{
	printf("test binary:\n");
	testBinary();

	printf("test fibbo\n");
	testFibonacci();

	return 0;
}
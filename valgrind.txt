==4824== Memcheck, a memory error detector
==4824== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==4824== Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
==4824== Command: ./fibonacci_heap_test
==4824== Parent PID: 35
==4824== 
==4824== error calling PR_SET_PTRACER, vgdb might block
==4824== 
==4824== HEAP SUMMARY:
==4824==     in use at exit: 0 bytes in 0 blocks
==4824==   total heap usage: 328,092 allocs, 328,092 frees, 28,908,296 bytes allocated
==4824== 
==4824== All heap blocks were freed -- no leaks are possible
==4824== 
==4824== For counts of detected and suppressed errors, rerun with: -v
==4824== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
